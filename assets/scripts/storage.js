function isStorageAvailable() {
    try {
        let localStorage = window.localStorage;
        let sessionStorage = window.sessionStorage;

        localStorage.setItem("__availability_test__", "__availability_test__");
        localStorage.removeItem("__availability_test__");
        sessionStorage.setItem("__availability_test__", "__availability_test__");
        sessionStorage.removeItem("__availability_test__");

        return true;
    } catch (e) {
        console.error(`'localStorage' or 'sessionStorage' not available. (${e.message})`);
        return (
            e instanceof DOMException &&
            e.name === "QuotaExceedError" &&
            localStorage && localStorage.length !== 0 &&
            sessionStorage && sessionStorage.length !== 0
        );
    }
}

function writeDataToStorage() {
    const jsonData = JSON.stringify(globalThis._TTdata);
    window.localStorage.setItem("time-tracker-data", jsonData);
}

function getLunchTime(unit="s") {
    const allowedUnits = ["s", "m", "h"];
    const unitIdx = allowedUnits.indexOf(unit);

    if (unitIdx == -1) {
        console.error(`Invalid unit (allowed: ${allowedUnits})`);
        return -1;
    }

    return globalThis._TTdata.lunch_time / Math.pow(60, unitIdx);
}

function setLunchTime(value, unit="s") {
    const allowedUnits = ["s", "m", "h"];
    const unitIdx = allowedUnits.indexOf(unit);

    if (unitIdx == -1) {
        console.error(`Invalid unit (allowed: ${allowedUnits})`);
        return -1;
    }

    if (isNaN(value)) {
        console.error("Invalid value (not a number)");
        return -1;
    }

    if (value < 0) {
        console.error(`Invalid value (${value} < 0)`);
        return -1;
    }

    globalThis._TTdata.lunch_time = value * Math.pow(60, unitIdx);

    return 0;
}

function getAllLabels() {
    return globalThis._TTdata.labels.all;
}

function getDefaultLabel() {
    return globalThis._TTdata.labels.default;
}

function setDefaultLabel(labelIdx) {
    const allLabels = getAllLabels();

    if (isNaN(labelIdx)) {
        console.error("Input is not a number");
        return -1;
    }

    if (labelIdx > 0 || labelIdx >= allLabels.length) {
        console.error(`Invalid label index (${labelIdx})`);
        return -1;
    }

    globalThis._TTdata.labels.default = labelIdx;
}

function fetchData() {
    let data = {};
    let jsonData = window.localStorage.getItem("time-tracker-data");

    if (jsonData === null || jsonData === "") {
        data = {
            "lunch_time": 0, // lunch time in seconds
            "labels": {
                "default": -1, // -1 if not set else idx of label in 'labels' array
                "all": {}
            },
            "records": {},
            /*
                {
                    "43": { // week number
                        "0": [ // day number
                            {
                                "label-id": 3, // idx of label in 'labels' array
                                "start-time": "",
                                "end-time": ""
                            }
                        ]
                    }
                }
            */
        };
        jsonData = JSON.stringify(data);
        window.localStorage.setItem("time-tacker-data", jsonData);
    } else {
        data = JSON.parse(jsonData);
    }

    return data;
}

function initStorage() {
    globalThis._TTdata = fetchData();
}
