function addDay(date, showRecordBtn) {
    const dayContainer = document.createElement("div");
    const dateContainer = document.createElement("div");
    const dayInputsContainer = document.createElement("div");
    const timeRegisterBtnContainer = document.createElement("div");

    const dateSpan = document.createElement("span");

    const labelSelector = createLabelSelector();

    dayContainer.classList.add("day-container");
    dateContainer.classList.add("date-container");
    dayInputsContainer.classList.add("day-inputs-container");
    timeRegisterBtnContainer.classList.add("time-register-btn-container");

    dateSpan.classList.add("date");

    dateContainer.appendChild(dateSpan);

    if (showRecordBtn) {
        const timeRegisterBtn = createRecordBtn();
        timeRegisterBtnContainer.appendChild(timeRegisterBtn);
    } else {
        // TODO
        // const totalTime = document.createElement("span");
    }

    dayInputsContainer.appendChild(timeRegisterBtnContainer);
    dayInputsContainer.appendChild(labelSelector);

    dayContainer.appendChild(dateContainer);
    dayContainer.appendChild(dayInputsContainer);

    return dayContainer;
}

/**
 * @param {Date[7]} week
 * @param {number} today Index of today in `week`
 */
function populateWeekContainer(week, dayIdx) {
    const weekContainer = document.getElementsByClassName("week-container")[0];

    for (let i = 0; i < 7; i++) {
        const day = addDay(week[i], (i == dayIdx));

        weekContainer.appendChild(day);

        if (i != 6) {
            const daySeparator = document.createElement("div");
            daySeparator.classList.add("day-separator");
            weekContainer.appendChild(daySeparator);
        }
    }
}

function writeDate(week) {
    const allDate = document.getElementsByClassName("date");

    for (let i = 0; i < allDate.length; i++) {
        allDate[i].innerText = getFullDateStr(week[i]);
    }
}

function scrollDown() {
    const weekContainer = document.getElementsByClassName("week-container")[0];
    weekContainer.scrollIntoView(true);
}

function pageInit() {
    scrollDown();

    if (!isStorageAvailable()) {
        console.error("Storage API must be avaialble");
    }

    initStorage();

    const today = new Date();
    const week = getWeek(today);
    const dayIdx = getDay(today);

    populateWeekContainer(week, dayIdx);
    writeDate(week);
}
