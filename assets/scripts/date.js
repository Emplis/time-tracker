function getDay(date) {
    return (date.getDay() == 0) ? 6 : date.getDay() - 1; // 0 - 6 = Sun. - Sat. -> 0 - 6 = Mon. - Sun.
}

function getWeek(date) {
    const dayIdx = getDay(date);

    const mon = new Date(date);
    const tue = new Date(date);
    const wed = new Date(date);
    const thu = new Date(date);
    const fri = new Date(date);
    const sat = new Date(date);
    const sun = new Date(date);

    mon.setDate(date.getDate() - dayIdx);
    tue.setDate(mon.getDate() + 1);
    wed.setDate(tue.getDate() + 1);
    thu.setDate(wed.getDate() + 1);
    fri.setDate(thu.getDate() + 1);
    sat.setDate(fri.getDate() + 1);
    sun.setDate(sat.getDate() + 1);

    const week = [mon, tue, wed, thu, fri, sat, sun];

    return week;
}

function getDayStr(date) {
    const daysStr = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"];
    const dayIdx = getDay(date);

    return daysStr[dayIdx];
}

function getShortDayStr(date) {
    const dayStr = getDayStr(date);
    const shortDayStr = dayStr.slice(0, 3);

    return shortDayStr;
}

function getMonthStr(date) {
    const monthsStr = ["january", "february", "march", "april", "may", "june", "july", "september", "october", "november", "december"];
    const monthIdx = date.getMonth();

    return monthsStr[monthIdx];
}

function getFullDateStr(date) {
    const firstLetterToUpperCase = (str) => {
        const firstLetter = str.charAt(0).toUpperCase();

        return firstLetter + str.slice(1);
    };

    const dayStr = firstLetterToUpperCase(getDayStr(date));
    const monthStr = firstLetterToUpperCase(getMonthStr(date));
    const dayOfMonth = date.getDate();
    const year = date.getFullYear();

    const dateStr = `${dayStr} ${dayOfMonth} ${monthStr} ${year}`;

    return dateStr;
}