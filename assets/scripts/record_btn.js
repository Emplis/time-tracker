function createRecordBtn(btnCallbackStart, btnCallbackStop) {
    const svgNameSpaceURI = "http://www.w3.org/2000/svg";
    const svg = document.createElementNS(svgNameSpaceURI, "svg");
    const circle = document.createElementNS(svgNameSpaceURI, "circle");
    const rect = document.createElementNS(svgNameSpaceURI, "rect");

    const btnDataAttr = "started";

    const viewBoxWidth = 100;
    const viewBoxHeight = viewBoxWidth;
    const animationDuration = "0.2s";

    const cx = 50;
    const cy = cx;
    const strokeWidth = 6;
    const radius = (viewBoxWidth / 2) - (strokeWidth / 2);

    const rectStartWidth = 80;
    const rectStartHeight = rectStartWidth;
    const rectStartX = (viewBoxWidth / 2) - (rectStartWidth / 2);
    const rectStartY = (viewBoxHeight / 2) - (rectStartHeight / 2);
    const rectStartCornerRadius = "50%";

    const rectEndWidth = 40;
    const rectEndHeight = rectEndWidth;
    const rectEndX = (viewBoxWidth / 2) - (rectEndWidth / 2);
    const rectEndY = (viewBoxHeight / 2) - (rectEndHeight / 2);
    const rectEndCornerRadius = "5%";

    const circleAttr = {
        "cx": cx,
        "cy": cy,
        "r": radius,
        "stroke": "black",
        "stroke-width": strokeWidth,
        "fill-opacity": 0
    };

    const rectStartAttr = {
        "x": rectStartX,
        "y": rectStartY,
        "rx": rectStartCornerRadius,
        "width": rectStartWidth,
        "height": rectStartHeight,
        "fill": "black",
    };

    const rectEndAttr = {
        "x": rectEndX,
        "y": rectEndY,
        "rx": rectEndCornerRadius,
        "width": rectEndWidth,
        "height": rectEndHeight,
        "fill": "black",
    };

    svg.setAttribute("viewBox", `0 0 ${viewBoxWidth} ${viewBoxHeight}`);
    svg.setAttribute("xmlns", svgNameSpaceURI);
    svg.setAttribute(`data-${btnDataAttr}`, "false");
    svg.setAttribute("tabindex", "0");
    svg.classList.add("time-register-btn");

    const circleAttrKeys = Object.keys(circleAttr);
    for (let i = 0; i < circleAttrKeys.length; i++) {
        const key = circleAttrKeys[i];
        circle.setAttribute(key, `${circleAttr[key]}`);
    }
    circle.classList.add("svg-outer-obj");

    const rectAttrKeys = Object.keys(rectStartAttr);
    for (let i = 0; i < rectAttrKeys.length; i++) {
        const key = rectAttrKeys[i];
        rect.setAttribute(key, `${rectStartAttr[key]}`);
    }
    rect.classList.add("svg-inner-obj");

    let animationsIn = [];
    let animationsOut = [];

    for (let i = 0; i < 2; i++) {
        const direction = (i == 0) ? "in" : "out";
        for (let j = 0; j < rectAttrKeys.length; j++) {
            const key = rectAttrKeys[j];

            if (rectStartAttr[key] != rectEndAttr[key]) {
                const animate = document.createElementNS(svgNameSpaceURI, "animate");

                const attributeName = key;
                let attributeValues = "";
                
                if (direction == "in") {
                    animationsIn.push(animate);

                    attributeValues = `${rectStartAttr[key]};${rectEndAttr[key]}`;
                } else {
                    animationsOut.push(animate);

                    attributeValues = `${rectEndAttr[key]};${rectStartAttr[key]}`;
                }

                animate.setAttribute("attributeName", attributeName);
                animate.setAttribute("begin", "indefinite");
                animate.setAttribute("values", attributeValues);
                animate.setAttribute("dur", animationDuration);
                animate.setAttribute("fill", "freeze");
                animate.classList.add(`svg-anim-${direction}-${attributeName}`);

                rect.appendChild(animate);
            }
        }
    }

    svg.appendChild(circle);
    svg.appendChild(rect);

    svg.addEventListener("click", () => {
        let clicked = (svg.getAttribute(`data-${btnDataAttr}`) == "true");

        if (!clicked) {
            animationsIn.forEach(animation => {
                animation.beginElement();
            });
            svg.setAttribute(`data-${btnDataAttr}`, "true");
            btnCallbackStart();
        } else {
            animationsOut.forEach(animation => {
                animation.beginElement();
            });
            svg.setAttribute(`data-${btnDataAttr}`, "false");
            btnCallbackStop();
        }
    });

    return svg;
}
