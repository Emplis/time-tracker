function createLabelSelector(selectCallback) {
    const select = document.createElement("select");
    const optionTitle = document.createElement("option");
    const optionNewLabel = document.createElement("option");

    const allLabels = getAllLabels();
    const defaultLabel = getDefaultLabel();

    select.classList.add("label-selector");
    select.setAttribute("title", "Click to select for which to register time for");

    optionTitle.value = "";
    optionTitle.disabled = true;
    optionTitle.innerText = "Labels";

    optionNewLabel.value = `${allLabels.length}`;
    optionNewLabel.innerHTML = "Create a new label...";

    select.appendChild(optionTitle);

    for (let i = 0; i < allLabels.length; i++) {
        const label = allLabels[i];
        const option = document.createElement("option");

        option.value = `${i}`;
        option.innerText = label;

        if (i === defaultLabel) {
            option.selected = true;
        }

        select.appendChild(option);
    }

    if (allLabels.length === 0 || defaultLabel === -1) {
        optionTitle.selected = true;
    }

    select.appendChild(optionNewLabel);

    select.addEventListener("change", () => {
        selectCallback(select.value);
    });

    return select;
}
